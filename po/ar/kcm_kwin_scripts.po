# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2015.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-22 02:45+0000\n"
"PO-Revision-Date: 2022-09-20 20:34+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "استورد سكرِبت نوافذك"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|سكرِبتات نوافذك (*.kwinscript)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"لا يمكن استيراد السكربت المحدد.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "استورد السّكرِبت \"%1\" بنجاح."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "خطأ عند إلغاء تثبيت سكربت كوين: %1"

#: package/contents/ui/main.qml:47
#, kde-format
msgctxt "@info:tooltip"
msgid "Delete..."
msgstr "احذف..."

#: package/contents/ui/main.qml:60
#, kde-format
msgid "Install from File..."
msgstr "ثبت من ملف..."

#: package/contents/ui/main.qml:64
#, kde-format
msgid "Get New Scripts..."
msgstr "احصل على سكرِبت جديد..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "صفا الفليج"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "safa1996alfulaij@gmail.com"

#~ msgid "KWin Scripts"
#~ msgstr "سكرِبتات نوافذك"

#~ msgid "Configure KWin scripts"
#~ msgstr "اضبط سكرِبتات نوافذك"

#~ msgid "Tamás Krutki"
#~ msgstr "Tamás Krutki"

#~ msgid "KWin script configuration"
#~ msgstr "إعداد سكرِبت نوافذك"

#~ msgid "Import KWin script..."
#~ msgstr "استورد سكرِبت نوافذك..."

#~ msgid ""
#~ "Cannot import selected script: maybe a script already exists with the "
#~ "same name or there is a permission problem."
#~ msgstr ""
#~ "تعذّر استيراد السّكرِبت المحدّد: قد يكون موجودًا بالفعل باسم آخر أو توجد مشكلة "
#~ "في الصّلاحيّات."
